# unifi



## Update unifi controller

(Install or) update the unifi controller application on ubuntu / debian hosts.
Will be pulled recurring by ansible-pull and executed on change. 
When a new controller version is available, just change the **unifi_url** variable in **vars/unifi.yml** and the update will be deployed automatically.

## Instructions

* Check the unifi controller version you want to install and edit the unifi_url to point to the corresponding .deb file.
* On the host, install ansible and git
* ansible-pull -U $repourl
* Wait until it's finished and log on to https://host:8443
* Once installed, it will check the code every ten minutes. If changed it will be pulled and deployed.
* Updating the controller is as easy as changing the unifi_url and wait a few minutes. 

## Tested

Tested on Debian 12 bookwork

## Additional comment
Unfortunately we have to install libssl1.1 as a mongodb dependency from the focal security repository